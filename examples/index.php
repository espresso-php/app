<?php

use Espresso\App\Espresso;
use Espresso\Responder\Psr\PsrResponder;
use function Http\Response\send;
use Zend\Diactoros\ResponseFactory;
use Zend\Diactoros\ServerRequestFactory;
use Zend\Diactoros\StreamFactory;
use Zend\Diactoros\UriFactory;

require_once __DIR__ . '/../vendor/autoload.php';

// We create an instance of Espresso passing a callable for generating a server request
// and another for emitting a response.
$app = new Espresso(
    [ServerRequestFactory::class, 'fromGlobals'],
    function ($response) {
        send($response);
    }
);

// NOTE: You can optionally provide a container instance as a third argument.

// We create a Responder. This is optional. Is for making responses easily.
$responder = new PsrResponder(
    new ResponseFactory(),
    new StreamFactory(),
    new UriFactory()
);

// We define the home page route.
$app->get('/', function () use ($responder) {
    return $responder->json([
        'msg' => 'This is the home page'
    ]);
});

// Espresso allows us to create routable http components that can be injected in the
// main app. Here we create a user module.
$userModule = Espresso::router();
// We define some routes relative to this module.
$userModule->get('/', function ($req) use ($responder) {
    return $responder->json([
        'msg' => 'This is a list of users',
        'path' => $req->getUri()->getPath()
    ]);
});
$userModule->get('/:id', function ($req) use ($responder) {
    return $responder->json([
        'msg' => 'The user id is '.$req->getAttribute('id'),
        'path' => $req->getUri()->getPath(),
    ]);
});
// Then we inject it in the app
$app->use('/users', $userModule);

// We can create another module.
$userModule2 = Espresso::router();
$userModule2->get('/namespace/:id', function ($req) use ($responder) {
    return $responder->json([
        'msg' => 'The user id is '.$req->getAttribute('id'),
        'note' => 'This is the namespaced handler',
        'path' => $req->getUri()->getPath(),
    ]);
});
// We inject it in the application too. We can even use the same path than the other.
$app->use('/users', $userModule2);

// We define a "catch all" that will return a 404 if no middleware was able to produce a response.
$app->use(function () use ($responder) {
    return $responder->json([
        'msg' => 'Not found',
        'note' => 'This handler is located at the end of all the middleware chain',
    ], 404);
});

$app->run();