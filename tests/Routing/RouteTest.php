<?php

namespace Espresso\App\Tests\Routing;

use Espresso\App\Middleware\EspressoMiddleware;
use Espresso\App\Frame\EspressoFrame;
use Espresso\App\Routing\Route;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;

class RouteTest extends TestCase
{
    public function testMatches(): void
    {
        $responseMock = $this->createMock(ResponseInterface::class);
        $requestMock = $this->createMock(ServerRequestInterface::class);
        $uriMock = $this->createMock(UriInterface::class);
        $handlerMock = $this->createMock(EspressoMiddleware::class);

        $uriMock->expects($this->once())
            ->method('getPath')
            ->willReturn('/users/23532532');
        $requestMock->expects($this->once())
            ->method('getUri')
            ->willReturn($uriMock);
        $requestMock->expects($this->once())
            ->method('getMethod')
            ->willReturn('GET');
        $requestMock->expects($this->once())
            ->method('withAttribute')
            ->with('id')
            ->willReturn($requestMock);

        $handlerMock->expects($this->once())
            ->method('execute')
            ->with($requestMock, $frameMock)
            ->willReturn($responseMock);

        $route = new Route(['GET'], '/users/:id', $handlerMock);
        $response = $route->execute($requestMock, $frameMock);
    }
}
