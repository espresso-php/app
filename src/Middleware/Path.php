<?php

namespace Espresso\App\Middleware;

use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface as Next;
use Zend\Stratigility\Exception\EmptyPipelineException;
use Zend\Stratigility\MiddlewarePipe;

/**
 * Class PathMiddlewareDecorator
 *
 * Description of what this class does goes here.
 *
 * @author Matias Navarro Carter <mnavarro@option.cl>
 */
class Path implements MiddlewareInterface
{
    use PathMatcherTrait;
    /**
     * @var MiddlewareInterface
     */
    private $middleware;

    /**
     * PathMiddlewareDecorator constructor.
     * @param string $path
     * @param MiddlewareInterface ...$middleware
     */
    public function __construct(string $path, MiddlewareInterface ...$middleware)
    {
        if (count($middleware) === 0) {
            throw new InvalidArgumentException('You must provide at least one middleware');
        }
        $this->buildRegex($path);
        $this->middleware = new MiddlewarePipe();
        foreach ($middleware as $mid) {
            $this->middleware->pipe($mid);
        }
    }

    /**
     * @param Request $request
     * @param Next $next
     * @return Response
     */
    public function process(Request $request, Next $next): Response
    {
        try {
            return $this->match($request, $this->middleware);
        } catch (UnmatchedPathException $e) {
            return $next->handle($request);
        } catch (EmptyPipelineException $e) {
            return $next->handle($request);
        }
    }
}