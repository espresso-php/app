<?php

namespace Espresso\App\Middleware;

use Exception;

/**
 * Class UnmatchedException
 *
 * Description of what this class does goes here.
 *
 * @author Matias Navarro Carter <mnavarro@option.cl>
 */
class UnmatchedPathException extends Exception
{
    public function __construct()
    {
        parent::__construct('Unmatched path');
    }
}