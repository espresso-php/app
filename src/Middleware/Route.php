<?php

namespace Espresso\App\Middleware;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface as Next;
use Zend\Stratigility\Exception\EmptyPipelineException;
use Zend\Stratigility\MiddlewarePipe;


/**
 * Class Route
 *
 * This middleware represents a route. It is only executed if it
 * matches.
 *
 * @author Matias Navarro Carter <mnavarro@option.cl>
 */
class Route implements MiddlewareInterface
{
    use PathMatcherTrait;

    /**
     * @param string $path
     * @param MiddlewareInterface ...$handlers
     * @return Route
     */
    public static function get(string $path, MiddlewareInterface ...$handlers): Route
    {
        return new self(['GET'], $path, ...$handlers);
    }

    /**
     * @param string $path
     * @param MiddlewareInterface ...$handlers
     * @return Route
     */
    public static function post(string $path, MiddlewareInterface ...$handlers): Route
    {
        return new self(['POST'], $path, ...$handlers);
    }

    /**
     * @param string $path
     * @param MiddlewareInterface ...$handlers
     * @return Route
     */
    public static function put(string $path, MiddlewareInterface ...$handlers): Route
    {
        return new self(['PUT'], $path, ...$handlers);
    }

    /**
     * @param string $path
     * @param MiddlewareInterface ...$handlers
     * @return Route
     */
    public static function patch(string $path, MiddlewareInterface ...$handlers): Route
    {
        return new self(['PATCH'], $path, ...$handlers);
    }

    /**
     * @param string $path
     * @param MiddlewareInterface ...$handlers
     * @return Route
     */
    public static function delete(string $path, MiddlewareInterface ...$handlers): Route
    {
        return new self(['DELETE'], $path, ...$handlers);
    }

    /**
     * @var array
     */
    private $methods;
    /**
     * @var MiddlewareInterface
     */
    private $handler;

    /**
     * Route constructor.
     * @param array $methods
     * @param string $path
     * @param MiddlewareInterface[] $handlers
     */
    public function __construct(array $methods, string $path, MiddlewareInterface ...$handlers)
    {
        $this->methods = $methods;
        $this->buildRegex($path);
        $this->handler = new MiddlewarePipe();
        foreach ($handlers as $handler) {
            $this->handler->pipe($handler);
        }
    }

    /**
     * @param Request $request
     * @param Next $next
     * @return Response
     */
    public function process(Request $request, Next $next): Response
    {
        if (!$this->matchesMethod($request)) {
            return $next->handle($request);
        }

        try {
            return $this->match($request, $this->handler);
        } catch (UnmatchedPathException $e) {
            return $next->handle($request);
        } catch (EmptyPipelineException $e) {
            return $next->handle($request);
        }
    }

    /**
     * @param Request $request
     * @return bool
     */
    protected function matchesMethod(Request $request): bool
    {
        return empty($this->methods) || in_array($request->getMethod(), $this->methods, true);
    }
}