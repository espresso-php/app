<?php

namespace Espresso\App\Middleware;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\UriInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface as Next;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Trait PathMatcherTrait
 *
 * Description of what this class does goes here.
 *
 * @author Matias Navarro Carter <mnavarro@option.cl>
 */
trait PathMatcherTrait
{
    /**
     * @var string
     */
    private $regex = '';
    /**
     * @var array
     */
    private $variables = [];

    /**
     * @param Request $request
     * @param RequestHandlerInterface $handler
     * @return Response
     * @throws UnmatchedPathException
     */
    protected function match(Request $request, RequestHandlerInterface $handler): Response
    {
        // First, we get the real or the extracted uri
        if (($uri = $request->getAttribute(UriInterface::class)) === null) {
            $uri = $request->getUri();
        }

        // if the path is blank, we must make it "/"
        if ($uri->getPath() === '' ) {
            $uri = $uri->withPath('/');
        }

        // Second, we evaluate if the uri matches the regular expression.
        $matches = [];
        if (!((bool) preg_match($this->regex, $uri->getPath(), $matches))) {
            throw new UnmatchedPathException();
        }

        // Third, if we are here, we have a match. So we build the params array.
        $fullMatch = array_shift($matches);
        // If we have any vars, we inject them into the request.
        if (count($matches) > 0) {
            foreach (array_combine($this->variables, $matches) as $key => $value) {
                $request = $request->withAttribute($key, $value);
            }
        }
        // Fourth, if we are dealing with a route, fullmatch must be exaclty equal to
        // the passed uri path.
        if ($this instanceof Route && $fullMatch !== $uri->getPath()) {
            throw new UnmatchedPathException();
        }

        // Fifth, we remove the full match from the uri to create a new path.
        // That new path is passed in a request param so next handlers know what they
        // have to match against.
        $newPath = str_replace($fullMatch, '', $uri->getPath());
        $request = $request->withAttribute(UriInterface::class, $uri->withPath($newPath));

        // Finally, we pass everything to the provided handler.
        return $handler->handle($request);
    }

    /**
     * Builds the regex expression for the given path and stores any variable that
     * finds.
     *
     * @param string $path
     * @return void
     */
    private function buildRegex(string $path): void
    {
        if ($path === '/') {
            $this->regex = '/\//';
            return;
        }
        $path = trim($path, '/');
        $this->regex = '/';
        $routeData = explode('/', $path);
        foreach ($routeData as $part) {
            if ($this->isVariable($part)) {
                $this->regex .= '\/([^\/]+)';
                $this->variables[] = str_replace(':', '', $part);
                continue;
            }
            $this->regex .= '\/'.$part;
        }

        $this->regex .= '/';
    }

    /**
     * @param $part
     * @return bool
     */
    private function isVariable($part): bool
    {
        return strpos($part, ':') === 0;
    }
}