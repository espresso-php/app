<?php

namespace Espresso\App;

use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Interface UsesMiddleware
 *
 * Description of what this class does goes here.
 *
 * @author Matias Navarro Carter <mnavarro@option.cl>
 */
interface UsesMiddleware
{
    /**
     * Pipes one or more middleware into the execution chain.
     *
     * @param string|callable|MiddlewareInterface|RequestHandlerInterface ...$middleware
     */
    public function use(...$middleware): void;
}