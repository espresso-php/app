<?php

namespace Espresso\App;

use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Espresso\App\Middleware\Route;

/**
 * Interface RegistersRoutes
 *
 * Description of what this interface is for goes here.
 *
 * @author Matias Navarro Carter <mnavarro@option.cl>
 */
interface RegistersRoutes
{
    /**
     * Registers a handler for a GET uri
     *
     * @param string $path
     * @param string|MiddlewareInterface|RequestHandlerInterface|callable ...$middleware
     * @return Route
     */
    public function get(string $path, ...$middleware): Route;

    /**
     * Registers a handler for a POST uri
     *
     * @param string $path
     * @param string|MiddlewareInterface|RequestHandlerInterface|callable ...$middleware
     * @return Route
     */
    public function post(string $path, ...$middleware): Route;

    /**
     * Registers a handler for a PUT uri
     *
     * @param string $path
     * @param string|MiddlewareInterface|RequestHandlerInterface|callable ...$middleware
     * @return Route
     */
    public function put(string $path, ...$middleware): Route;

    /**
     * Registers a handler for a PATCH uri
     *
     * @param string $path
     * @param string|MiddlewareInterface|RequestHandlerInterface|callable ...$middleware
     * @return Route
     */
    public function patch(string $path, ...$middleware): Route;

    /**
     * Registers a handler for a DELETE uri
     *
     * @param string $path
     * @param string|MiddlewareInterface|RequestHandlerInterface|callable ...$middleware
     * @return Route
     */
    public function delete(string $path, ...$middleware): Route;

    /**
     * Registers a handler for a uri that matches any verb
     *
     * @param string $path
     * @param string|MiddlewareInterface|RequestHandlerInterface|callable ...$middleware
     * @return Route
     */
    public function any(string $path, ...$middleware): Route;
}