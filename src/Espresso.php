<?php

namespace Espresso\App;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Throwable;

/**
 * This is the Espresso class.
 *
 * Espresso is just a PSR-15 Request Handler with routing and middleware
 * piping capabilities. It is also capable of emitting responses if an
 * implementation of Responder is provided.
 *
 * @package Espresso\App
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class Espresso extends RoutableHttpComponent implements RequestHandlerInterface
{
    /**
     * @var callable
     */
    private $requestBuilder;
    /**
     * @var callable
     */
    private $responseEmitter;

    /**
     * Espresso constructor.
     * @param callable $requestBuilder
     * @param callable $responseEmitter
     * @param ContainerInterface|null $container
     */
    public function __construct(callable $requestBuilder, callable $responseEmitter, ContainerInterface $container = null)
    {
        parent::__construct();
        $this->requestBuilder = $requestBuilder;
        $this->responseEmitter = $responseEmitter;
        self::$container = $container;
    }

    /**
     * Returns an empty instance of a Routable HTTP Component
     *
     * @return RoutableHttpComponent
     */
    public static function router(): RoutableHttpComponent
    {
        return new RoutableHttpComponent();
    }

    /**
     * Handles a PSR-7 ServerRequest and returns a PSR-7 Response
     *
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws Throwable
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return $this->middlewarePipe->handle($request);
    }

    public function run(): void
    {
        $request = ($this->requestBuilder)();
        $response = $this->handle($request);
        ($this->responseEmitter)($response);
    }
}