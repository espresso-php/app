<?php

/*
 * This file is part of the Hermes\HttpApp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Espresso\App;

use Closure;
use InvalidArgumentException;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class ResolverTrait.
 *
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
trait ResolverTrait
{
    /**
     * @var ContainerInterface|null
     */
    protected static $container;

    /**
     * @param string|callable|MiddlewareInterface|RequestHandlerInterface $middleware
     *
     * @return MiddlewareInterface
     */
    protected function resolve($middleware): MiddlewareInterface
    {
        if ($middleware instanceof MiddlewareInterface) {
            return $middleware;
        }
        if ($middleware instanceof RequestHandlerInterface) {
            return Decorator::handler($middleware);
        }
        if ($middleware instanceof Closure) {
            return Decorator::callable($middleware);
        }
        if (is_string($middleware) && null !== self::$container && self::$container->has($middleware)) {
            return Decorator::lazy(self::$container, $middleware);
        }
        throw new InvalidArgumentException(sprintf('Cannot resolve "%s"', $middleware));
    }

    /**
     * @param array $middleware
     * @return array
     */
    protected function resolveArray(array $middleware): array
    {
        return array_map(function ($mid) {
            return $this->resolve($mid);
        }, $middleware);
    }
}
