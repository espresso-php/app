<?php

namespace Espresso\App;

use Espresso\App\Middleware\LazyMiddleware;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Stratigility\Middleware as Zend;
use Zend\Stratigility\MiddlewarePipe;

/**
 * This class defines a set of static methods to decorate different types
 * as middleware, or decorate middleware itself to be ran in certain circumstances.
 *
 * @package Espresso\App
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class Decorator
{
    /**
     * Decorates a middleware to make it lazy. Needs a ContainerInterface.
     *
     * @param ContainerInterface $container
     * @param string $middlewareService
     * @return LazyMiddleware
     */
    public static function lazy(ContainerInterface $container, string $middlewareService): LazyMiddleware
    {
        return new LazyMiddleware($container, $middlewareService);
    }

    /**
     * Decorates a PSR-15 Request Handler to make it a middleware
     *
     * @param RequestHandlerInterface $handler
     * @return Zend\RequestHandlerMiddleware
     */
    public static function handler(RequestHandlerInterface $handler): Zend\RequestHandlerMiddleware
    {
        return new Zend\RequestHandlerMiddleware($handler);
    }

    /**
     * Decorates a middleware to be ran on a path
     *
     * @param MiddlewareInterface $middleware
     * @param string              $path
     * @return Zend\PathMiddlewareDecorator
     */
    public static function path(MiddlewareInterface $middleware, string $path): Zend\PathMiddlewareDecorator
    {
        return new Zend\PathMiddlewareDecorator($path, $middleware);
    }

    /**
     * Decorates a middleware to be executed on a specific host.
     *
     * @param MiddlewareInterface $middleware
     * @param string              $host
     * @return Zend\HostMiddlewareDecorator
     */
    public static function host(MiddlewareInterface $middleware, string $host): Zend\HostMiddlewareDecorator
    {
        return new Zend\HostMiddlewareDecorator($host, $middleware);
    }

    /**
     * Wraps a callable in a middleware
     *
     * @param callable $middleware
     * @return Zend\CallableMiddlewareDecorator
     */
    public static function callable(callable $middleware): Zend\CallableMiddlewareDecorator
    {
        return new Zend\CallableMiddlewareDecorator($middleware);
    }
}