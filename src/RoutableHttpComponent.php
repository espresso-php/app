<?php

namespace Espresso\App;

use Espresso\App\Middleware\Path;
use Espresso\App\Middleware\Route;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface as Next;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Stratigility\MiddlewarePipe;
use Zend\Stratigility\MiddlewarePipeInterface;

/**
 * Class HttpComponent
 *
 * Description of what this class does goes here.
 *
 * @author Matias Navarro Carter <mnavarro@option.cl>
 */
class RoutableHttpComponent implements MiddlewareInterface, RegistersRoutes, UsesMiddleware
{
    use ResolverTrait;
    /**
     * @var MiddlewarePipeInterface
     */
    protected $middlewarePipe;

    /**
     * HttpComponent constructor.
     */
    public function __construct()
    {
        $this->middlewarePipe = new MiddlewarePipe();
    }

    /**
     * @param Request $request
     * @param RequestHandlerInterface $next
     * @return Response
     */
    public function process(Request $request, Next $next): Response
    {
        return $this->middlewarePipe->process($request, $next);
    }

    /**
     * @param string|MiddlewareInterface|RequestHandlerInterface|callable ...$middleware
     */
    public function use(...$middleware): void
    {
        $handler = null;
        if (count($middleware) <= 0) {
            throw new InvalidArgumentException(sprintf('Method use of %s expects at least one argument. None given.', __CLASS__));
        }
        if (is_string($middleware[0]) && !empty($middleware[0]) && strpos($middleware[0], '/') === 0) {
            $handler = new Path(array_shift($middleware), ...$this->resolveArray($middleware));
            $this->middlewarePipe->pipe($handler);
            return;
        }
        foreach ($middleware as $mid) {
            $this->middlewarePipe->pipe($this->resolve($mid));
        }
    }

    /**
     * @param string $path
     * @param mixed ...$middleware
     * @return Route
     */
    public function get(string $path, ...$middleware): Route
    {
        $route = Route::get($path, ...$this->resolveArray($middleware));
        $this->middlewarePipe->pipe($route);
        return $route;
    }

    /**
     * @param string $path
     * @param mixed ...$middleware
     * @return Route
     */
    public function post(string $path, ...$middleware): Route
    {
        $route = Route::post($path, ...$this->resolveArray($middleware));
        $this->middlewarePipe->pipe($route);
        return $route;
    }

    /**
     * @param string $path
     * @param mixed ...$middleware
     * @return Route
     */
    public function put(string $path, ...$middleware): Route
    {
        $route = Route::put($path, ...$this->resolveArray($middleware));
        $this->middlewarePipe->pipe($route);
        return $route;
    }

    /**
     * @param string $path
     * @param mixed ...$middleware
     * @return Route
     */
    public function patch(string $path, ...$middleware): Route
    {
        $route = Route::patch($path, ...$this->resolveArray($middleware));
        $this->middlewarePipe->pipe($route);
        return $route;
    }

    /**
     * @param string $path
     * @param mixed ...$middleware
     * @return Route
     */
    public function delete(string $path, ...$middleware): Route
    {
        $route = Route::delete($path, ...$this->resolveArray($middleware));
        $this->middlewarePipe->pipe($route);
        return $route;
    }

    /**
     * @param string $path
     * @param mixed ...$middleware
     * @return Route
     */
    public function any(string $path, ...$middleware): Route
    {
        $route = new Route([], $path, ...$this->resolveArray($middleware));
        $this->middlewarePipe->pipe($route);
        return $route;
    }
}