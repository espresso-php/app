Espresso
========

PHP with a cup of coffee

## Introduction
Espresso is a minimal HTTP layer for your web application built with PHP.
It is heavily inspired in Node's Express.

We aim to make PHP development as simple as writing a Node Js app, while using
PHP awesome object oriented features and FIG standards for maximum interoperability.
We take some decisions for you while still allowing maximum tweaking so you can sit,
enjoy your cup of coffee and rest assured this framework will be up to the task.

To know more about middleware, PSR-7 and related things, check this
[awesome blog](https://mwop.net/blog/2015-01-08-on-http-middleware-and-psr-7.html)
by Matthew Weier O'Phinney, project lead of Zend Framework (now Laminas Project).

## Installation 
To install Espresso, simply run:

```
composer require espresso/app
```

Or, if you prefer an skeleton project with some directory structure you can use:

```
composer create-project espresso/skeleton <folder>
```

Please refer to the skeleton documentation to know how to use it.

## Usage
Check the examples folder to see how to use Espresso. The api is really similar to the one
of Express JS.

## Common HTTP Plugins

### Body Parser

### Cookie

### Proxies